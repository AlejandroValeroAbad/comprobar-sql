function ejecutarSentencias(){
    document.getElementById('ejercicios').innerHTML = "";
    console.log("Funcionando");
    let file = document.getElementById('xml').files[0];
    let parser = new DOMParser();
    let reader = new FileReader();
    reader.onload = function(event){
        let textoXML = event.target.result;
        let archivoXML = parser.parseFromString(textoXML, "text/xml");
        let nodo;

        let resultadoXPATH = document.evaluate('//ejercicios/ejercicio', archivoXML, null, XPathResult.ANY_TYPE, null)
        let sentencias = [];
        while(nodo = resultadoXPATH.iterateNext()){
            sentencias.push((nodo.textContent));
        }

        resultadoXPATH = document.evaluate('//ejercicios/ejercicio/@id', archivoXML, null, XPathResult.ANY_TYPE, null)
        let identificadores = [];
        while(nodo = resultadoXPATH.iterateNext()){
            identificadores.push((nodo.textContent));
        }

        resultadoXPATH = document.evaluate('//ejercicios/ejercicio/@peso', archivoXML, null, XPathResult.ANY_TYPE, null)
        let puntuaciones = [];
        while(nodo = resultadoXPATH.iterateNext()){
            puntuaciones.push((nodo.textContent));
        }

        resultadoXPATH = document.evaluate('//ejercicios/@practica', archivoXML, null, XPathResult.ANY_TYPE, null)
        nodo = resultadoXPATH.iterateNext();
        let nombrePractica = nodo.textContent;

        document.getElementById('ejercicios').innerHTML += "<br/><b class='titulo'>" + nombrePractica + "</b>"
        
        for(let i = 0; i<sentencias.length; i++){
            url = 'http://137.74.226.47:8080/practicas/'
            url += nombrePractica + '/';
            url += identificadores[i];
            console.log(url);
            fetch(url, {
                method: 'POST',
                body: sentencias[i]
              })
              .then(function(response){
                if(response.ok){
                  return response.text();
                }else{
                  throw "ERROR";
                }
            }).then(function(texto){
                let valor = texto.substring(texto.indexOf(":")+2, texto.lastIndexOf("\"}")-3);
                let ejercicio = valor.substring(0, valor.indexOf(".")-2);
                ejercicio = ejercicio.replace("cicio", "cicio ");
                let tiempo =  valor.substring(valor.indexOf("[")+1);
                tiempo = tiempo.substring(0, tiempo.indexOf("]"));
                tiempo = "[" + tiempo + "]";
                if(valor.indexOf("ejecución") != -1){
                    valor = valor.substring(valor.indexOf("INCORRECTO"));
                }else if(valor.indexOf("SENTENCIA") != -1){
                    valor = valor.substring(valor.indexOf("SENTENCIA"));
                }else{
                    valor = valor.replace(tiempo, "");
                    if(valor.indexOf("OK") != -1){
                        valor = "OK";
                    }else{
                        valor = valor.substring(valor.indexOf("["));
                    }
                }
                  document.getElementById('ejercicios').innerHTML += "<br/><b class='ejercicio'>" + ejercicio + "</b><br/><b class='query'>" + valor + "</b><br/>";
                  
            }).catch(function(err) {
                console.log(err);
            })
                          

        }
    }
    reader.readAsText(file, "ISO-8859-1");
}